#include "Enigma.h"

#define GETARRAYSIZE(x) (sizeof(x) / sizeof(x[0]))
#define CONVERT(diget, pos) ((diget)*pow(16, (pos)))

Enigma::Enigma(SupportedEncryption supportedEncryption) : Logable("Enigma")
{
    #ifdef ARDUINO
    this->usedEncryption = supportedEncryption;
    switch (usedEncryption)
    {
    case SupportedEncryption::aes128:
        cipher = new AES128;
        break;
    case SupportedEncryption::aes256:
        // cipher = new AES256;
        cipher = new AES256_ESP;
        break;
    }
    #endif
}

Enigma::~Enigma()
{
}
#ifdef ARDUINO
void Enigma::hexStringTo8ByteArray(String hexString, byte buffer[8])
{
    int length = hexString.length();
    char temp[2];

    for (int i = 0; i < length; i = i + 2)
    {
        temp[0] = hexString.charAt(i);
        temp[1] = hexString.charAt(i + 1);
        buffer[i / 2] = charToByte(temp);
    }
}

void Enigma::hexStringTo16ByteArray(String hexString, byte buffer[16])
{
    int length = hexString.length();
    char temp[2];

    for (int i = 0; i < length; i = i + 2)
    {
        temp[0] = hexString.charAt(i);
        temp[1] = hexString.charAt(i + 1);
        buffer[i / 2] = charToByte(temp);
    }
}

void Enigma::hexStringToByteArray(String hexString, byte *buffer)
{
    int length = hexString.length();
    if (length % 2 != 0)
    {
        buffer = nullptr;
        return;
    }
    char temp[2];
    buffer = new byte[length / 2];
    for (int i = 0; i < length; i = i + 2)
    {
        temp[0] = hexString.charAt(i);
        temp[1] = hexString.charAt(i + 1);
        buffer[i / 2] = charToByte(temp);
    }
}

byte Enigma::charToByte(char digets[2])
{
    // this is the length of the char array -1
    static byte highesPos = 1;
    byte result = 0;
    for (int i = 0; i < 2; i++)
    {
        switch (digets[i])
        {
        case '0':
            break;
        case '1':
            result += CONVERT(1, highesPos - i);
            break;
        case '2':
            result += CONVERT(2, highesPos - i);
            break;
        case '3':
            result += CONVERT(3, highesPos - i);
            break;
        case '4':
            result += CONVERT(4, highesPos - i);
            break;
        case '5':
            result += CONVERT(5, highesPos - i);
            break;
        case '6':
            result += CONVERT(6, highesPos - i);
            break;
        case '7':
            result += CONVERT(7, highesPos - i);
            break;
        case '8':
            result += CONVERT(8, highesPos - i);
            break;
        case '9':
            result += CONVERT(9, highesPos - i);
            break;
        case 'A':
            result += CONVERT(10, highesPos - i);
            break;
        case 'B':
            result += CONVERT(11, highesPos - i);
            break;
        case 'C':
            result += CONVERT(12, highesPos - i);
            break;
        case 'D':
            result += CONVERT(13, highesPos - i);
            break;
        case 'E':
            result += CONVERT(14, highesPos - i);
            break;
        case 'F':
            result += CONVERT(15, highesPos - i);
            break;
        }
    }
    return result;
}

void Enigma::hashData(char *key, char *data, uint8_t result[HASH_SIZE])
{
    memset(result, 0xAA, sizeof(result));
    hmac<SHA256>(result, HASH_SIZE, key, strlen(key), data, strlen(data));
}

void Enigma::stringToByteArray(String cString, byte *buffer)
{
    cString.getBytes(buffer, cString.length() + 1);
}

String Enigma::byteArrayToString(const byte *bytearray, int arraysize)
{
    String result((char *)bytearray);
    return result;
}

byte *Enigma::setKey(char *key)
{
    byte byteKey[HASH_SIZE];
    this->log("Hashing key",Loglevel::LOWLEVELDEBUG);
    hashData("", key, byteKey);
    this->byteKey = byteKey;
    this->log("Saving key",Loglevel::LOWLEVELDEBUG);
    cipher->setKey(this->byteKey, 32);
    return this->byteKey;
}

// plaintext and cypher max 16 bytes more will get cut off
void Enigma::encrypt(byte plaintext[16], byte cypher[16])
{
    // cypher->output block and plaintext->input block
    cipher->encryptBlock(cypher, plaintext);
}
// decryptedtext and cypher max 16 bytes more will get cut off
void Enigma::decrypt(byte cypher[16], byte decryptedtext[16])
{
    // decryptedtext->output block and cypher->input block
    cipher->decryptBlock(decryptedtext, cypher);
}

int Enigma::calculateBlockSize(String data)
{
    // Calculate right size
    int finalSize = data.length() + 1;
    finalSize = finalSize + 16 - finalSize % 16;
    return finalSize;
}

byte *Enigma::prepareStringForEncryption(String data, byte *preparedData, bool escapeReservedSymbol)
{

    // Calculate right size
    int finalSize = calculateBlockSize(data);

    preparedData = new byte[finalSize];
    // Serial.println("Converted to byte");

    stringToByteArray(data,preparedData);
    for (int i = data.length(); i < finalSize; i++)
    {
        preparedData[i] = fillerChar;
    }

    return preparedData;
}

int Enigma::removeEscapeSymbols(String rawstring, byte *result)
{
    // byte result [rawstring.length()];
    int resultIndex = 0;
    for (int i = 0; i < rawstring.length(); i++)
    {
        if (rawstring[i] == escapeChar && i + 1 < rawstring.length())
        {
            if (rawstring[i + 1] == escapeChar)
            {
                result[resultIndex] = escapeChar;
                i++;
            }
            else if (rawstring[i + 1] == fillerChar)
            {
                result[resultIndex] = fillerChar;
                i++;
            }
            else if (i + 3 < rawstring.length())
            {
                char charBuffer[4] = {rawstring[i + 1], rawstring[i + 2], rawstring[i + 3], '\0'};

                
                String number = charBuffer;
                result[resultIndex] = number.toInt();//+94;
                 
                i += 3;
            }
        }
        else
        {
            result[resultIndex] = rawstring[i];
        }
        Serial.print((char)result[resultIndex]);
        resultIndex++;
    }
    Serial.println();
    // Remove additional \0
    // resultIndex--;
    return resultIndex;
}

// If you use a string do not forget to include the terminating 0
// This means size = string.size +1
String Enigma::escapeReservedSymbols(byte *rawstring, int size)
{
    char buffer[4];
    String result = "";
    bool foundNotAllowedChar = false;
    for (int n = 0; n < size; n++)
    {

        for (int i = 0; i < GETARRAYSIZE(notAllowedCharsInString); i++)
        {
            if (rawstring[n] == notAllowedCharsInString[i])
            {
                foundNotAllowedChar = true;

                
                itoa(notAllowedCharsInString[i], buffer, 10);

                // 0 in buffer is a seperator "terminating zero". A one diget number has a 0 on pst buffer[1]. And a 2 Diget number has a 0 at buffer[2]
                // If there is a 0 at buffer[1] -> remove it and move all buffer parts one cell to the right
                // Fill first cell with the char '0'
                if (buffer[1] == 0)
                {
                    buffer[2] = buffer[1];
                    buffer[1] = buffer[0];
                    buffer[0] = '0';
                }
                // Repeat it if the buffer[2] is 0
                if (buffer[2] == 0)
                {
                    buffer[2] = buffer[1];
                    buffer[1] = buffer[0];
                    buffer[0] = '0';
                }
                // buffer[3] = 0;
                result += Enigma::escapeChar;
                result += (char *)buffer;

                break;
            }
        }
        if (!foundNotAllowedChar)
        {
            result += (char)(rawstring[n]);
        }
        foundNotAllowedChar = false;
    }
    return result;
}

String Enigma::processStringAfterDecryption(byte *data, int size)
{
    // Remove filler chars
    int fillerIndex = 0;
    for (fillerIndex = size; fillerIndex >= 0; fillerIndex--)
    {
        // This will fail if last character in the string is a filler char
        // first char is null so skip it
        if (data[fillerIndex-1] == fillerChar)
        {
            data[fillerIndex]=0;
        }
        // else if (data[fillerIndex] == fillerChar)
        else
        {
            break;
        }
    }
    // Removing last fillerchar
    data[fillerIndex]=0;

    String onlyData = (char *)data;


    return onlyData;
}

String Enigma::encrypt(String plaintext)
{
    // Calculate data size
  int finalSize = calculateBlockSize(plaintext);
  // Prepare String
  byte *byteData = prepareStringForEncryption(plaintext, byteData, true);

  // Create buffer for encrypted result
  byte encryptedData[finalSize];

  // encrpyt
  crypt(byteData, finalSize, encryptedData, true);

//   Serial.print("Escaped Encrypted String:");
  String cypher = escapeReservedSymbols(encryptedData, finalSize);

  return cypher;
}

String Enigma::decrypt(String cypher)
{
    int finalSize = calculateBlockSize(cypher);
  byte decryptedData[finalSize];
  int resultSize = removeEscapeSymbols(cypher, decryptedData);
  // Create buffer for decrypted message
  byte *originalMessage = new byte[resultSize];
  crypt(decryptedData, resultSize, originalMessage, false);

  String plaintext = processStringAfterDecryption(originalMessage, resultSize);
//   Serial.print("Plaintext:");
//   Serial.println(plaintext);
  return plaintext;
}

void Enigma::crypt(byte *source, int length, byte *result, bool encryptIt)
{
    // Serial.println(source[0]);
    byte plaintextBuffer[BLOCKSIZE];
    byte processedtextBuffer[BLOCKSIZE];

    byte bufferIndex = 0;
    int resultIndex = 0;

    for (int i = 0; i < length; i++)
    {
        if (bufferIndex == 15)
        {
            plaintextBuffer[bufferIndex] = source[i];

            if (encryptIt == true)
            {
                encrypt(plaintextBuffer, processedtextBuffer);
            }
            else
            {
                decrypt(plaintextBuffer, processedtextBuffer);
            }
            // Write proccessed data to result
            for (int n = 0; n < BLOCKSIZE; n++)
            {
                result[resultIndex] = processedtextBuffer[n];
                resultIndex++;
            }
            bufferIndex = 0;
        }
        else
        {
            plaintextBuffer[bufferIndex] = source[i];
            bufferIndex++;
        }
    }
}
#endif