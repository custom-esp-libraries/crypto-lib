#include ".settings.h"
#include "Logger.h"
#include "Logable.h"
#ifdef ARDUINO
#include "Crypto/AES.h"
#include "Crypto/Crypto.h"
#include <Arduino.h>
#include <string.h>

#include "Crypto/SHA256.h"

#pragma once

#define HASH_SIZE 32
#define BLOCKSIZE 16
#endif
// Current limitations only a key of 16 chars can beused with this lib
// And only 16 chars can get encrypted at once more result in errors
// So this wrapper solves this problem
// TODO add more encryptions methodes
enum SupportedEncryption
{
    aes128,
    aes256
};



class Enigma : public Logable 
{
    
public:
    Enigma(SupportedEncryption supportedEncryption);
    ~Enigma();
    #ifdef ARDUINO
    byte *setKey(char *key);

    void hashData(char *key,char *data, uint8_t result[HASH_SIZE]);

    String byteArrayToString(const byte *bytearray, int arraysize);
    // plaintext and cypher max 16 bytes more will get cut off
    void encrypt(byte plaintext[16], byte cypher[16]);
    // decryptedtext and cypher max 16 bytes more will get cut off
    void decrypt(byte cypher[16], byte decryptedtext[16]);

    String encrypt(String plaintext);
    String decrypt(String cypher);
    // String crypt(String source, bool encrypt);
    void crypt(byte *source, int length, byte *result, bool encryptIt);
    void stringToByteArray(String cString, byte *buffer);
    String escapeReservedSymbols(String rawstring);
    String removeEscapeSymbols(String rawstring);
    String escapeReservedSymbols(byte *rawstring, int size);
    // String removeEscapeSymbols(byte* rawstring);
    // Returns size of the actuall result string
    int removeEscapeSymbols(String rawstring, byte *result);
    String processStringAfterDecryption(byte *data, int size);
    // data is the data which needs to be prepared for the encryption or after decryption
    // prepareForEncryption = true If the string should be used for encryption
    // prepareForEncryption = false If the string was decrypted and now it needs to get the fillerchars and so removed so it is again the normal string
    // byteData is the result which gets returned
    // Returns size of byteData
    byte *prepareStringForEncryption(String data, byte *preparedData, bool escapeReservedSymbols);
    int calculateBlockSize(String data);
    static const char fillerChar = '~';
    // Do not use \ as filler char or escape char because it can create new errors
    static const char escapeChar = '#';

private:
    byte buffer[16];
    BlockCipher *cipher;
    SupportedEncryption usedEncryption;

    // convert written chararray to byte e.g. FF is converted to 255 and 1F is converted to 31
    byte charToByte(char digets[2]);
    // the hexString is a human readable hexstring somethinglike 18 F3 E9 8A CD 03 90 3B will get converted to 24 243 233 138 etc
    // The max string size is currently 16 chars = buffer(size)*2
    void hexStringTo8ByteArray(String hexString, byte buffer[8]);
    void hexStringTo16ByteArray(String hexString, byte buffer[16]);
    void hexStringToByteArray(String hexString, byte *buffer);

    // char fillerChar='f';
    // char escapeChar='e';

    byte *byteKey;
};

const byte notAllowedCharsInString[] = {
    0,          // NUL (null)
    1,          // SOH (start of heading)
    2,          // STX (start of text)
    3,          // ETX (end of text)
    4,          // EOT (end of transmission)
    5,          // ENQ (enquiry)
    6,          // ACK (acknowledge
    7,          // BEL (bell)
    8,          // BS (backspace)
    9,          // TAB (horizontal tab)
    10,         // LF (NL line feed, new line)
    11,         // VT (vertical tab)
    12,         // FF (NP form feed, new page)
    13,         // CR (carriage return)
    14,         // SO (shift out)
    15,         // SI (shift in)
    16,         // DLE (data link escape)
    17,         // DC1 (device control 1)
    18,         // DC2 (device control 2)
    19,         // DC3 (device control 3)
    20,         // DC4 (device control 4)
    21,         // NAK (negative acknowledge)
    22,         // SYN (synchronous idle)
    23,         // ETB (end of trans. block)
    24,         // CAN (cancel)
    25,         // EM (end of medium)
    26,         // SUB (substitute)
    27,         // ESC (escape)
    28,         // FS (file separator)
    29,         // GS (group separator)
    30,         // RS (record separator)
    31,         // US (unit separator)
    // 32,         // SP
    34,         // "
    127,        // DEL
    128,		// 
    129,		// 
    130,		// 
    131,		// 
    132,		// 
    133,		// 
    134,		// 
    135,		// 
    136,		// 
    137,		// 
    138,		// 
    139,		// 
    140,		// 
    141,		// 
    142,		// 
    143,		// 
    144,		// 
    145,		// 
    146,		// 
    147,		// 
    148,		// 
    149,		// 
    150,		// 
    151,		// 
    152,		// 
    153,		// 
    154,		// 
    155,		// 
    156,		// 
    157,		// 
    158,		// 
    159,		// 
    160,		//  
    161,		// ¡
    162,		// ¢
    163,		// £
    164,		// ¤
    165,		// ¥
    166,		// ¦
    167,		// §
    168,		// ¨
    169,		// ©
    170,		// ª
    171,		// «
    172,		// ¬
    173,		// ­
    174,		// ®
    175,		// ¯
    176,		// °
    177,		// ±
    178,		// ²
    179,		// ³
    180,		// ´
    181,		// µ
    182,		// ¶
    183,		// ·
    184,		// ¸
    185,		// ¹
    186,		// º
    187,		// »
    188,		// ¼
    189,		// ½
    190,		// ¾
    191,		// ¿
    192,		// À
    193,		// Á
    194,		// Â
    195,		// Ã
    196,		// Ä
    197,		// Å
    198,		// Æ
    199,		// Ç
    200,		// È
    201,		// É
    202,		// Ê
    203,		// Ë
    204,		// Ì
    205,		// Í
    206,		// Î
    207,		// Ï
    208,		// Ð
    209,		// Ñ
    210,		// Ò
    211,		// Ó
    212,		// Ô
    213,		// Õ
    214,		// Ö
    215,		// ×
    216,		// Ø
    217,		// Ù
    218,		// Ú
    219,		// Û
    220,		// Ü
    221,		// Ý
    222,		// Þ
    223,		// ß
    224,		// à
    225,		// á
    226,		// â
    227,		// ã
    228,		// ä
    229,		// å
    230,		// æ
    231,		// ç
    232,		// è
    233,		// é
    234,		// ê
    235,		// ë
    236,		// ì
    237,		// í
    238,		// î
    239,		// ï
    240,		// ð
    241,		// ñ
    242,		// ò
    243,		// ó
    244,		// ô
    245,		// õ
    246,		// ö
    247,		// ÷
    248,		// ø
    249,		// ù
    250,		// ú
    251,		// û
    252,		// ü
    253,		// ý
    254,		// þ
    255,		// ÿ
    
    Enigma::escapeChar, // Char used to escape reserved symbols
    Enigma::fillerChar  // Used to fillup unused space
    #endif
};

